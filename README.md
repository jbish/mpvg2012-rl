Deep reinforcement learning for Ms. Pac-Man vs. Ghosts 2012.

This directory is the Origin trainer for the System.
Here is a brief overview of the subdirectories and files contained within:

# Subdirectories

## build
Contains compiled Java code for the game.

## data
Contains data for use in the game engine, such as maze information and
character sprites.

## eval_results
Stores all evaluation results that were produced, in the form of Slurm output
logs.  Each subdirectory represents a specific trial of an agent, with the name of the subdirectory indicating the model name and tag used, as well as the trial number.
E.g.
'google_atari_vanilla-lr_0.0001-0' means the model was google_atari_vanilla,
tag was lr_0.0001 and it was the first trial (trial 0).

Also contains Python scripts to analyse these evaluation ouptut logs and
produced learning curves.
All produced learning curve graphics are also included.

## game_images
Non-important directory, was just used to store images from the game for use in
the thesis document.

## src
Contains source code for both the agent and the game.
All code under the 'pacman' directory is for the game, and the .py files are
for the agent.

## visualisation
Contains some code that was used in an attempt to produce weight activation
images for the agent CNN.

## weight_calcs
Contains a python script, 'weight_calcs.py' that was used to produce the
weights evolution plots.
All produced weights evolution plots are also included.


# Files

build.sh
Shell script to build the game code. Makes use of sources.txt to determine
which files to compile.

## eval.sh
Script for an evaluator. This script would be placed in an 'mpvg2012-rl-eval'
directory, and appropriate symlinks created to the trainer directory (as explained in the thesis document)
in order to create an evaluator.

## eval_random.sh
Slurm script specifically for evaluating random agents.

## human_results.txt
Contains raw results of the human performance study

## mult_run.sh
Launcher script for training.

## notes.txt
Miscellaneous notes from project work.

## run.sh
Main runner script, called from the launcher script.

## sources.txt
List of sources for the build.sh script
