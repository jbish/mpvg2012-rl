#!/bin/bash
# set the number of nodes
#SBATCH --nodes=1
# set number of cpus
#SBATCH --cpus-per-task=4

# set max wallclock time
#SBATCH --time=2-00:00:00

# set parition and gpu info
#SBATCH --partition="gpu"
#SBATCH --gres=gpu:1

# Test for all weights in the given directories redirecting stdout and stderr of
# java process to a file.

export DISPLAY=:7

MODEL_NAME="google_atari_vanilla"
TAG_BASE="lr_0.0001"

NUM_RUNS=4
NUM_EVALS=10

tags=()
for ((i=0; i<${NUM_RUNS}; i++)); do
    tags[$i]=${TAG_BASE}-$i
done

for tag in ${tags[@]}; do
    # create dir for results
    mkdir -p eval_results/${MODEL_NAME}-${tag}
    weights_dir=$(pwd)/network_weights/${MODEL_NAME}-${tag}
    i=1
    for file in ${weights_dir}/*.h5; do
        # Ignore initial weights
        if [[ ${file} != *"init"* ]]; then
            export THEANO_FLAGS="floatX=float32,device=gpu,nvcc.fastmath=True,\
            lib.cnmem=1,compiledir=~/.theano/"${MODEL_NAME}-${tag}

            for ((j=0; j<${NUM_EVALS}; j++)); do
                Xvfb ${DISPLAY} -screen 0 250x300x16 & XVFB_PID=$!
                java -cp build pacman.Executor false 0 ${MODEL_NAME} ${tag} ${file} \
                >> eval_results/${MODEL_NAME}-${tag}/${MODEL_NAME}-${tag}-weights-${i}-eval.txt 2>&1
                # Kill the Xvfb process so it can be started again with the
                # static display number.
                kill $XVFB_PID
            done
            i=$((i+1))
        fi
    done
done
