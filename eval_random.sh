#!/bin/bash
# set the number of nodes
#SBATCH --nodes=1
# set number of cpus
#SBATCH --cpus-per-task=4

# set max wallclock time
#SBATCH --time=2:00:00

NUM_EVALS=50
TAG="no_reverse"

mkdir -p eval_results/random

for ((i=0; i<${NUM_EVALS}; i++)); do
    java -cp build pacman.Executor false 0 random ${TAG} \
    >> eval_results/random/random-${TAG}-eval.txt 2>&1
done
