import os
import numpy as np
import parse as ps


def get_eval_data(dir_list, num_weights, evals_per_weight, sort_string):
    score_data = np.zeros((len(dir_list), num_weights, evals_per_weight))
    time_data = np.zeros((len(dir_list), num_weights, evals_per_weight))
    i = 0
    for directory in dir_list:
        j = 0
        files = os.listdir(directory)
        files.sort(key=lambda f: int(ps.parse(sort_string, f)[1]))
        for fi in files:
            k = 0
            fp = open(os.path.join(directory, fi), 'r')
            for line in fp:
                if "Game over!" in line:
                    info = ps.parse("PYTHON: Game over! score = {}, time = {}",
                                    line)
                    score = int(info[0])
                    time = int(info[1])
                    score_data[i][j][k] = score
                    time_data[i][j][k] = time
                    k += 1
            fp.close()
            j += 1
        i += 1
    return score_data, time_data
