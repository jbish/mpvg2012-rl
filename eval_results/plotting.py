#!/usr/bin/python
"""Create matplotlib graphs of eval results."""
import files
import numpy as np
import matplotlib
matplotlib.use('Agg')  # Plot without displaying
import matplotlib.pyplot as plt

if __name__ == "__main__":

    model_name = "google_atari_dropout_fc"
    tag = "drop_25_priority_replay_lr_0.0001_1200k_exp_600k_epsilon"
    num_dirs = 3

    replay_mem_size = 150000
    num_train_exps = 8*replay_mem_size
    num_weights = 40
    weight_save_freq = num_train_exps/num_weights
    evals_per_weight = 10

    dir_list = \
        ["{0}-{1}-{2}".format(model_name, tag, i) for i in range(0, num_dirs)]
    sort_string = model_name + "-" + tag + "-{}-weights-{}-eval.txt"

    score_data, time_data = files.get_eval_data(dir_list, num_weights,
                                                evals_per_weight, sort_string)
    # get mean score and time data
    mean_score_data = np.mean(score_data, 2)
    mean_time_data = np.mean(time_data, 2)

    experiences = \
        np.asarray([(weight_save_freq*i)/1000 for i in range(1, num_weights+1)])

    # plot score data
    for i in range(0, len(dir_list)):
        plt.plot(experiences, mean_score_data[i])
    # set x axis bounds
    axes = plt.gca()
    axes.set_xlim([experiences[0], experiences[-1]])
    # adjust x axis ticks
    plt.xticks(experiences[::4])
    # set axis labels and title
    plt.xlabel("Number of experiences (thousands)")
    plt.ylabel("Mean score")
    plt.title("Priority Replay Agent Learning Curve (p = 0.75, LR = 0.0001)")
    plt.savefig("{0}-{1}-agent_learning_curve.png".format(model_name, tag))
    #plt.show()
