#!/bin/bash
dirbase="google_atari_dropout_fc-drop_25_lr_0.0005"
for((i=0; i<6; i++)); do
    dir=$dirbase-$i
    for f in $dir/*; do
        name=${f##*/}
        mv ${f} $dir/${name/vanilla/google_atari}
    done
    for f in $dir/*; do
        name=${f##*/}
        mv ${f} ${dir}/${name/_25+lr_0.0005/-drop_25_lr_0.0005}
    done
done
