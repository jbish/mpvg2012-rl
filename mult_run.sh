#!/bin/bash
START_DISPLAY=1
for ((i=0; i<6; i++)); do
	sbatch --job-name="mpvg2012-$i" run.sh $((${START_DISPLAY}+$i)) $i
	sleep 5
done
