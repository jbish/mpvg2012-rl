#!/bin/bash
# set the number of nodes
#SBATCH --nodes=1
# set the number of cpus
#SBATCH --cpus-per-task=4

# set max wallclock time
#SBATCH --time=6-00:00:00

# set parition and gpu info
#SBATCH --partition="gpu"
#SBATCH --gres=gpu:1

MODEL_NAME="google_atari_vanilla"
TAG_BASE="priority_replay_lr_0.0001_1200k_exp_600k_epsilon"

REPLAY_MEM_SIZE=150000
MAX_TRAIN_EXPS=$((8*${REPLAY_MEM_SIZE}))

export THEANO_FLAGS="floatX=float32,device=gpu,nvcc.fastmath=True,lib.cnmem=1,\
compiledir=~/.theano/"${MODEL_NAME}-${TAG_BASE}-${2}

export DISPLAY=:$1

Xvfb :$1 -screen 0 250x300x16 & \
java -cp build pacman.Executor true ${MAX_TRAIN_EXPS} ${MODEL_NAME} \
${TAG_BASE}-$2
