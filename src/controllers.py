from __future__ import division
import collections
import params
import network
import random
import numpy as np
import os
import datetime
import sys


class TrainingController:

    """Training controller for the game."""

    def __init__(self, model_name, tag, weights_file):
        # Set initial values for game info
        self.score = 0
        self.lives = 3
        self.time = 0

        # Init the experience replay memory.
        if params.USE_PRIORITY_REPLAY:
            self.replay_memory = PriorityReplayMemory()
        else:
            self.replay_memory = ReplayMemory()

        # init networks
        self.training_network = network.Convnet(model_name)
        self.target_network = network.Convnet(model_name)
        # set weights if they were specified
        if weights_file is not None:
            sys.stderr.write("PYTHON: Loading weights for training\n")
            self.training_network.model.load_weights(weights_file)
            self.target_network.model.load_weights(weights_file)

        self.model_name = model_name
        self.tag = tag
        # save initial weights to disk
        self.save_network_weights(True)

        # Initial value for epsilon in epsilon-greedy policy
        self.epsilon = params.EPSILON_INIT

    def record_experience(self, current_state, action, lives, score,
                          time, next_state, terminal):
        reward = self.calculate_reward(lives, score, time)
        sys.stderr.write("PYTHON: Reward for this experience = {0}\n".
                         format(reward))
        experience = Experience(current_state, action, reward, next_state,
                                terminal)
        self.replay_memory.add_experience(experience)

    def calculate_reward(self, lives, score, time):
        """The reward function for the neural network.

        Currently this does not take into account time, and only gives clipped
        rewards, i.e. +1 for positive, -1 for negative, 0 for default reward.
        It probably should give rewards that actually represent the magnitude of
        score change, as this is a pretty integral part of the game; eating 4
        ghosts consecutively is way better than eating just 1.

        Possibly also penalise terminal states greatly...

        Short bouts of bad play should be penalised greatly.
        Long bouts should be rewarded.

        """
        # score can only increase, so this is +ve
        score_delta = score - self.score
        # lives could increase or decrease, so this could be +ve or -ve
        lives_delta = lives - self.lives
        # update score and lives
        self.score = score
        self.lives = lives
        # Determine the reward.
        reward = 0  # Give a default reward of 0.1 for staying alive
        if lives_delta < 0:
            reward = -1
        elif lives_delta > 0 or score_delta > 0:
            reward = 1
        return reward

    def update_exploration_prob(self, elapsed_experiences):
        # calibrate the number of experiences to be the number after the
        # initial exploration period
        elapsed_experiences -= params.NUM_EXPLORE_EXPS
        # check if this number is within the annealing range
        if elapsed_experiences <= params.END_EPSILON_ANNEAL:
            # elapsed_experiences give distance along x axis
            x_diff = elapsed_experiences
            # calculate how much epsilon should decrease from its starting value
            # m = (y2 - y1)/(x2 - x1) --> (y2 - y1) = m*(x2 - x1)
            y_diff = params.ANNEAL_GRADIENT*x_diff
            # adjust epsilon. Add y_diff as it will always be negative
            self.epsilon = params.EPSILON_INIT + y_diff
            if elapsed_experiences == params.END_EPSILON_ANNEAL:
                sys.stderr.write("PYTHON: epsilon has been annealed to its "
                                 "final value\n")

    def select_action(self, current_state):
        """Select the action to perform next in the game.

        This is done according to an epsilon-greedy policy.

        """
        rand = random.random()
        if rand <= self.epsilon:
            # select an action randomly
            action = random.randint(1, 5)
        else:
            # select an action according to the training network's prediction
            action = self.training_network.get_action(current_state, True)
        return action

    def update_network(self):
        experiences = self.replay_memory.sample_experiences()
        self.process_experiences(experiences)

    def process_experiences(self, experiences):
        """Update the network weights on a batch of experiences.

        This is where the main Q-learning update rule occurs.

        """
        # Determine states and targets for training
        states = np.zeros((params.EXP_BATCH_SIZE, params.IMGS_PER_STATE,
                           params.IMG_ROWS, params.IMG_COLS))
        targets = np.zeros((params.EXP_BATCH_SIZE, 5))
        for i in range(0, params.EXP_BATCH_SIZE):
            experience = experiences[i]

            # calculate the maximum return using the target network
            reward = experience.reward
            next_state = experience.end_state
            next_state_q_values = self.target_network.get_q_values(next_state)
            largest_q_value = np.max(next_state_q_values)
            predicted_return = reward + params.DISCOUNT_FACTOR*largest_q_value

            if experience.terminal:
                max_return = reward
            else:
                max_return = max(reward, predicted_return)

            # Construct the state and target for this experience
            current_state = experience.start_state
            states[i] = current_state
            current_state_q_values = \
                self.training_network.get_q_values(current_state)
            # Set the target to the current prediction
            targets[i] = current_state_q_values

            # Now update the target for the chosen action
            action = experience.action
            action_index = action - 1
            targets[i][action_index] = max_return

        # Do training on states and their targets
        self.training_network.model.train_on_batch(states, targets)

    def sync_target_network(self):
        # Copy the weights of the training network to the target network
        training_weights = self.training_network.model.get_weights()
        self.target_network.model.set_weights(training_weights)

    def save_network_weights(self, init):
        # create a directory for the model+tag if it does not already exist
        weights_dir = "network_weights/{0}-{1}".format(self.model_name,
                                                       self.tag)
        if not os.path.exists(weights_dir):
            os.mkdir(weights_dir)
        # save the weights of the training network
        time = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        if not init:
            self.training_network.model. \
                save_weights("{0}/{1}-{2}-{3}-weights.h5".
                             format(weights_dir, self.model_name, self.tag,
                                    time))
        else:
            # add 'init' to end of weights file name
            self.training_network.model. \
                save_weights("{0}/{1}-{2}-{3}-init_weights.h5".
                             format(weights_dir, self.model_name, self.tag,
                                    time))

    def reset_game_info(self):
        self.score = 0
        self.lives = 3
        self.time = 0


class EvalController:

    """Evaluation controller for the game."""

    def __init__(self, model_name, weights_file):
        # Create the specified model and set its weights
        self.eval_network = network.Convnet(model_name)
        self.eval_network.model.load_weights(weights_file)

    def select_action(self, current_state):
        """Select the action to perform next in the game.

        This is simply the prediction the evaluation network outputs for the
        current state.

        """
        action = self.eval_network.get_action(current_state, False)
        return action


def warmup_controller(controller):
    sys.stderr.write("PYTHON: Warming up controller...\n")
    # Generate a random dummy state
    dummy_state = np.random.random_sample(size=(params.IMGS_PER_STATE,
                                                params.IMG_ROWS,
                                                params.IMG_COLS))
    # convert to float32 datatype
    dummy_state = dummy_state.astype('float32')
    if isinstance(controller, TrainingController):
        # do a forward pass on both the training and target networks
        train_out = controller.training_network.get_q_values(dummy_state)
        controller.target_network.get_q_values(dummy_state)

        # do dummy backprop on the training network
        # reshape output array to add extra dimension at front
        train_out = np.reshape(train_out, (1,) + tuple(np.shape(train_out)))
        # reshape dummy state array
        dummy_state = \
            np.reshape(dummy_state, (1,) + tuple(np.shape(dummy_state)))
        controller.training_network.model.train_on_batch(dummy_state, train_out)
    elif isinstance(controller, EvalController):
        # only need to do a forward pass for the evaluation network
        controller.eval_network.get_q_values(dummy_state)

    sys.stderr.write("PYTHON: Controller warmed up!\n")
    # print out the current date and time
    time = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    sys.stderr.write("PYTHON: Current time is: {0}\n\n".format(time))


class Experience:
    """A class used to represent an experience (a single transition in the MDP).

    The reason this is a class is so that tuples don't have to be used in the
    replay memory, and each part of the experience (s, a, r, s') can be easily
    accessed as a field of an Experience object instead of using tuple indexing.

    """

    def __init__(self, start_state, action, reward, end_state, terminal):
        self.start_state = start_state
        self.action = action
        self.reward = reward
        self.end_state = end_state
        self.terminal = terminal  # indicates whether the end state is terminal

    def get_size(self):
        """Get the size of this Experience object in bytes.

        Useful for debugging memory issues.
        """
        size = self.start_state.nbytes + sys.getsizeof(self.action) + \
               sys.getsizeof(self.reward) + self.end_state.nbytes + \
               sys.getsizeof(self.terminal)
        return size


class ReplayMemory:
    """Single bucket sliding window replay memory."""

    def __init__(self):
        self.bucket = collections.deque(maxlen=params.REPLAY_MEM_SIZE)

    def add_experience(self, experience):
        self.bucket.append(experience)

    def sample_experiences(self):
        return random.sample(self.bucket, params.EXP_BATCH_SIZE)


class PriorityReplayMemory:
    """ Multiple bucket sliding window replay memory."""

    def __init__(self):
        self.positive_bucket = \
            collections.deque(maxlen=params.REPLAY_MEM_SIZE/3)
        self.neutral_bucket = \
            collections.deque(maxlen=params.REPLAY_MEM_SIZE/3)
        self.negative_bucket = \
            collections.deque(maxlen=params.REPLAY_MEM_SIZE/3)

    def add_experience(self, experience):
        # Add the experience to the appropriate bucket
        reward = experience.reward
        if reward > 0:
            self.positive_bucket.append(experience)
        elif reward == 0:
            self.neutral_bucket.append(experience)
        elif reward < 0:
            self.negative_bucket.append(experience)

    def sample_experiences(self):
        samples = []
        samples.extend(random.sample(self.positive_bucket,
                                     params.POS_BATCH_SIZE))
        samples.extend(random.sample(self.neutral_bucket,
                                     params.NEU_BATCH_SIZE))
        samples.extend(random.sample(self.negative_bucket,
                                     params.NEG_BATCH_SIZE))
        return samples
