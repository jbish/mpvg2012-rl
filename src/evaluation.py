#!/usr/bin/python
from __future__ import print_function
import images
import controllers
import params
import sys
import numpy as np
import datetime


if __name__ == "__main__":
    # print out the current date and time
    time = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    sys.stderr.write("PYTHON: Current time is: {0}\n".format(time))

    # Get the model name
    model_name = sys.stdin.readline().strip()

    # Get the name of the weights file to use for network initialisation
    weights_file = sys.stdin.readline().strip()
    sys.stderr.write("PYTHON: Using weights file: {0}\n".format(weights_file))

    controller = controllers.EvalController(model_name, weights_file)

    current_state = None  # initially the current state is empty
    action = None  # Initialise action
    terminal = False  # flag to indicate if episode finished

    # "warm up" the controller  before the main loop
    controllers.warmup_controller(controller)

    score = 0
    time = 0

    # Main MDP loop
    while True:
        if terminal:
            # Print out the score
            sys.stderr.write("PYTHON: Game over! score = {0}, time = {1}\n\n".
                             format(score, time))
            break

        # Get the next state
        next_state = np.zeros((params.IMGS_PER_STATE, params.IMG_ROWS,
                               params.IMG_COLS), dtype=np.float32)
        for i in range(0, params.IMGS_PER_STATE):
            try:
                img_data_list = images.get_img_data_list()
            except EOFError:
                # Java is dead, go straight to terminal check at the start of
                #  the while loop
                terminal = True
                continue
            img_data_array = images.process_img_data(img_data_list)
            next_state[i] = img_data_array

        try:
            lives = int(sys.stdin.readline().strip())
            score = int(sys.stdin.readline().strip())
            time = int(sys.stdin.readline().strip())
        except ValueError:
            # This will have been caused by readline() returning '' for any
            # of the 3 above statements, meaning java died.
            terminal = True
            continue

        # Check if the current state is a terminal state (no lives left)
        if lives == 0:
            terminal = True

        # Update the current state
        current_state = next_state

        # Determine the action to take
        action = controller.select_action(current_state)
        # Send action back to the game
        print(action)
        sys.stdout.flush()
