import numpy as np
from PIL import Image
import params
import sys


def get_img_data_list():
    img_data_list = []
    # read img data from stdin in chunks
    for i in range(0, params.NUM_CHUNKS):
        chunk = sys.stdin.readline().strip()
        if chunk == '':
            # Readline gave an empty string, so java must have sent EOF
            # This means it's time to break out of the main MDP loop
            raise EOFError
        chunk = chunk.split(',')
        try:
            chunk = [int(i) for i in chunk]  # This could raise ValueError
        except ValueError:
            sys.stderr.write("PYTHON: Value error when converting chunk. "
                             "Chunk has size {0} and elements {1}\n".
                             format(len(chunk), chunk))
            raise
        img_data_list.extend(chunk)
    return img_data_list


def process_img_data(img_data_list):
    # Construct a grayscale PIL Image of the data
    img = Image.new("L", (params.RAW_IMG_WIDTH, params.RAW_IMG_HEIGHT))
    img.putdata(img_data_list)

    #img.save('img_{0}.png'.format(img_tag))

    # Resize the PIL image to the desired dimensions (width, height)
    img = img.resize((params.IMG_COLS, params.IMG_ROWS), Image.ANTIALIAS)

    # Create ndarray to hold the pixel data for this image
    img_data_array = np.zeros((params.IMG_ROWS, params.IMG_COLS),
                              dtype=np.float32)

    # Populate the ndarray with the pixel data and return it
    # height = rows = y dimension
    # width = columns = x dimension
    for i in range(0, params.IMG_ROWS):
        for j in range(0, params.IMG_COLS):
            # use (j, i) in getpixel as PIL refers to the pixel using (x, y)
            # co-ords
            img_data_array[i][j] = img.getpixel((j, i))

    # normalise the image data
    img_data_array /= 255
    return img_data_array
