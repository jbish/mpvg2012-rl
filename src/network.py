from keras.models import Sequential
from keras.layers.core import Dense, Flatten, Dropout
from keras.layers.convolutional import Convolution2D
from keras.optimizers import Adam
import numpy as np
import params
import sys


class Convnet:

    """The convolutional neural network.

    This class provides methods to perform operations on the network
    irrespective of its structure/topography. A specific structure can be
    created by supplying the constructor with the desired model name.

    Methods:
    get_move: Pass a state to the network to predict the next action to be
    taken.

    """

    def __init__(self, model_name):
        """Construct a new CNN model.

        Args:
        images_per_state: The number of image frames that comprise the input to
        the network.
        img_rows: The number of rows (height) in the input frames.
        img_cols: The number of columns (width) in the input frames.
        model_name: A string which gives the name of the model to be be
        constructed.

        Returns:
        A CNN model as a keras.models.Sequential object.

        """
        if model_name == "google_atari_vanilla":
            self.model = google_atari_vanilla()
        elif model_name == "google_atari_dropout_fc":
            self.model = google_atari_dropout_fc()

    def get_q_values(self, state):
        # reshape the state array (add an extra dimension at the front)
        # keras will complain otherwise
        state = np.reshape(state, (1,) + tuple(np.shape(state)))
        q_values = self.model.predict(state)
        # reshape this array to get rid of extra first dimension
        q_values = np.reshape(q_values, 5)
        return q_values

    def get_action(self, current_state, training):
        """Predict the action to take for the current state and return it (as an
        integer).

        1 = Down
        2 = Left
        3 = Neutral
        4 = Right
        5 = Up

        """
        q_values = self.get_q_values(current_state)
        # Return the action with the highest Q value
        if training:
            sys.stderr.write("PYTHON Q-values of the current state are: {0}\n".
                             format(q_values))
        action = int(np.argmax(q_values)) + 1
        return action


def google_atari_vanilla():
    """Replica of the CNN architecture in the DQN paper."""
    model = Sequential()
    model.add(Convolution2D(32, 8, 8, subsample=(4, 4), border_mode='same',
                            activation='relu', init='he_normal',
                            input_shape=(params.IMGS_PER_STATE,
                                         params.IMG_ROWS, params.IMG_COLS)))
    model.add(Convolution2D(64, 4, 4, subsample=(2, 2), border_mode='same',
                            activation='relu', init='he_normal'))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode='same',
                            activation='relu', init='he_normal'))
    model.add(Flatten())
    model.add(Dense(512, activation='relu', init='he_normal'))
    model.add(Dense(5, init='he_normal'))
    adam = Adam(lr=params.NETWORK_LEARN_RATE)
    model.compile(loss='mse', optimizer=adam)
    return model


def google_atari_dropout_fc():
    """Replica of the CNN architecture in the DQN paper + dropout in fully
    connected layers."""
    model = Sequential()
    model.add(Convolution2D(32, 8, 8, subsample=(4, 4), border_mode='same',
                            activation='relu', init='he_normal',
                            input_shape=(params.IMGS_PER_STATE,
                                         params.IMG_ROWS, params.IMG_COLS)))
    model.add(Convolution2D(64, 4, 4, subsample=(2, 2), border_mode='same',
                            activation='relu', init='he_normal'))
    model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode='same',
                            activation='relu', init='he_normal'))
    model.add(Dropout(params.DROPOUT))
    model.add(Flatten())
    model.add(Dense(512, activation='relu', init='he_normal'))
    model.add(Dropout(params.DROPOUT))
    model.add(Dense(5, init='he_normal'))
    adam = Adam(lr=params.NETWORK_LEARN_RATE)
    model.compile(loss='mse', optimizer=adam)
    return model
