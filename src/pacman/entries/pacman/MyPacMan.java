package pacman.entries.pacman;

import pacman.controllers.Controller;
import pacman.game.Constants.MOVE;
import pacman.game.Game;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/*
 * This is the class you need to modify for your entry. In particular, you need to
 * fill in the getAction() method. Any additional classes you write should either
 * be placed in this package or sub-packages (e.g., game.entries.pacman.mypackage).
 */
public class MyPacMan extends Controller<MOVE> {

	// Dimensions to use when cropping the image
	// (1, 5) is top left corner
	// (226, 254) is bottom right corner
	private static final int START_X = 1;
	private static final int START_Y = 5;
	private static final int IMG_WIDTH = 225;
	private static final int IMG_HEIGHT = 249;
	private static final int NUM_CHUNKS = 75;
	private static final int CHUNK_SIZE = (IMG_WIDTH*IMG_HEIGHT)/NUM_CHUNKS;
	// (225*249)/75 = 747
	private static final int IMAGES_PER_STATE = 3; // images per MDP state

	private Process python;
    private BufferedReader reader;
    private BufferedWriter writer;
    /* Indicates whether the last state transitioned through was a terminal
     * state
     */
    private boolean terminal;
	/* Indicates whether the controller is in training mode or not */
	private boolean training;
	// Number of moves returned by Python
	private int moveCount;

	/**
	 * Create a new custom game controller.
	 * @param training Flag to indicate if the controller should be run in
	 *                    training mode (true) or evaluation mode (false).
	 * @param modelName The name of the convnet architecture to use for
	 *                    playing.
	 * @param tag The tagname for this controller (used to name output
	 *               weights dir).
     * @param weightsFile The name of the weights file to use for initialising
	 *                       the controller convnet. Required if training ==
	 *                       false, optional otherwise.
	 * @throws IOException If there was a problem creating the python CNN
	 * process or sending data to this process.
     */
	public MyPacMan(boolean training, String modelName, String tag,
					String weightsFile)
			throws IOException {
		// Call superclass constructor to setup default stuff
		super();

		this.training = training;
		terminal = false;
		moveCount = 0;

		// Start a new python process for the convnet
		ProcessBuilder pb;
		if (training) {
			System.out.println("Controller is operating in training mode.");
			pb = new ProcessBuilder("python", "src/training.py");
		} else {
			System.out.println("Controller is operating in evaluation mode.");
			pb = new ProcessBuilder("python", "src/evaluation.py");
		}
		// Inherit stderr of the python process so errors can be seen
		pb.redirectError(ProcessBuilder.Redirect.INHERIT);
		python = pb.start();

		// Setup streams for the process
		reader = new BufferedReader(new InputStreamReader
				(python.getInputStream()));
		writer = new BufferedWriter(new
				OutputStreamWriter(python.getOutputStream()));

		// Print out model info
		System.out.println("Model name = " + modelName + ", tag = " + tag);

		// Send the model name
		writer.write(modelName + "\n");

		// Send the model tag if training
		if (training) {
			writer.write(tag + "\n");
		}

		// If a weights file was supplied, send it, else send "None"
		if (weightsFile != null) {
			writer.write(weightsFile + "\n");
		} else {
			writer.write("None\n");
		}
		writer.flush();
	}

	public MOVE getMove(Game game, long timeDue) {
		if (python.isAlive()) {
			long startTime = System.currentTimeMillis();

			// Send game information
			if (training) {
				System.out.println("Sending state data... ");
			}
			try {
				// Send game screens that make up the MDP state
				for (int i = 0; i < IMAGES_PER_STATE; i++) {
					sendGameScreen(game);
				}
				/* Send other game information used to calculate rewards (lives,
				 * score, time)
				 */
				sendGameInfo(game);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Read the value that the python process returns
			if (training) {
				System.out.println("Reading returned value... ");
			}
			int returnValue = 3;
			try {
				returnValue = Integer.parseInt(reader.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Determine the action to take based on the return value
			MOVE move = null;
			switch (returnValue) {
				case 1:
					move = MOVE.DOWN;
					break;
				case 2:
					move = MOVE.LEFT;
					break;
				case 3:
					move = MOVE.NEUTRAL;
					break;
				case 4:
					move = MOVE.RIGHT;
					break;
				case 5:
					move = MOVE.UP;
					break;
			}
			long endTime = System.currentTimeMillis();
			if (training) {
				System.out.println("Action " + move + " selected in " +
						(endTime - startTime) + " milliseconds\n");
			}
			moveCount++;
			return move;
		} else {
			return MOVE.NEUTRAL;
		}
	}

	/**
	 * Sends the most recent game screen to the python process.
	 * @param game The current internal game representation as a Game object.
	 * @throws IOException If there was a problem sending data.
     */
	private void sendGameScreen(Game game) throws IOException {
		BufferedImage gameScreen = game.getGameView().getGameScreen();
		// Get pixel data from the game screen
		int[] pixelData = gameScreen.getRGB(START_X, START_Y, IMG_WIDTH,
				IMG_HEIGHT, null, 0, IMG_WIDTH);
		// Send the pixel data
		sendPixelData(pixelData);
	}

	/**
	 * Send the given pixel data to the python process.
	 * @param pixelData An integer array of game screen pixel data.
	 * @throws IOException If there was a problem sending data.
     */
	private void sendPixelData(int[] pixelData) throws IOException {
		// Iterate over the pixel data and send it
		int counter = 0;
		for (int i = 0; i < pixelData.length; i++) {
			// Get RGB values
			Color colour = new Color(pixelData[i]);
			int red = colour.getRed();
			int green = colour.getGreen();
			int blue = colour.getBlue();
			// Convert RGB to grayscale then send
			/* This uses the same luminosity weights as the MATLAB rgb2gray
			 * function
			 */
			int gray = (int)Math.round((0.2989*red + 0.5870*green +
					0.1140*blue)/3);
			// Convert to string before sending
			writer.write(Integer.toString(gray));
			counter++;
			if (counter == CHUNK_SIZE) {
				writer.write("\n");
				writer.flush();
				counter = 0;
			} else {
				writer.write(",");
			}
		}
	}

	/**
	 * Send additional game information to the python process.
	 * @param game The current internal game representation as a Game object.
	 * @throws IOException If there was a problem sending data.
     */
	private void sendGameInfo(Game game) throws IOException {
	    /* Before getting lives from game representation, check if the last
         * state was terminal.
         */
	    int lives = 0;
	    if (!terminal) {
	        // If last state wasn't terminal get num of lives from game rep
            lives = game.getPacmanNumberOfLivesRemaining();
        }
		writer.write(lives + "\n");

		// Send the game score and time
		int score = game.getScore();
		writer.write(score + "\n");
		int time = game.getCurrentLevelTime();
		writer.write(time + "\n");
		writer.flush();

        // Reset terminal flag
        terminal = false;
	}

    /**
     * Setter method for the terminal flag of this controller.
     * @param terminal A boolean value indicating whether the last state
     *                 transitioned through was a terminal state.
     */
	public void setTerminal(boolean terminal) {
	    this.terminal = terminal;
    }

    /**
	 * Getter method for the move count of this controller.
	 */
    public int getMoveCount() {
    	return moveCount;
	}
}
