"""Hyperparameters for deep reinforcement learning."""
# Running on the cluster Tesla K80 with 3 images per state and using the
# google_atari CNN, the controller generates roughly 285 experiences per minute.

# Number of images per MDP state
IMGS_PER_STATE = 3

# Size of raw images sent over stdin
RAW_IMG_WIDTH = 225
RAW_IMG_HEIGHT = 249

# Chunk params for reading in state data
NUM_CHUNKS = 75
CHUNK_SIZE = (RAW_IMG_WIDTH*RAW_IMG_HEIGHT)/NUM_CHUNKS

# Dimensions of CNN input images after resizing
IMG_ROWS = 84
IMG_COLS = 84

# Maximum number of experiences to store in the experience replay memory
REPLAY_MEM_SIZE = 150000
# Whether or not to use priority experience replay
USE_PRIORITY_REPLAY = False

# The number of experiences (batch size) to use when training the network
EXP_BATCH_SIZE = 32

# Prioritised experience replay params
POS_BATCH_SIZE = 12  # 37.5%
NEU_BATCH_SIZE = 8  # 25%
NEG_BATCH_SIZE = 12  # 37.5%

# Initial value of epsilon for epsilon-greedy policy in Q-learning
EPSILON_INIT = 1  # start off being purely exploratory

# Final value of epsilon for epsilon-greedy policy in Q-learning
EPSILON_FINAL = 0.1  # explore 10% of the time at the end of annealing

# Number of experiences to explore for at the beginning of play. After this
# many experiences, epsilon starts to get annealed.
# This must be greater than EXP_BATH_SIZE so there is enough experience in the
# replay memory to start learning.
NUM_EXPLORE_EXPS = 10000

# Number of experiences after the initial exploration period to stop annealing
# epsilon at
END_EPSILON_ANNEAL = REPLAY_MEM_SIZE

# Gradient of the annealing line: m = (y2 - y1)/(x2 - x1)
ANNEAL_GRADIENT = (EPSILON_FINAL - EPSILON_INIT) / END_EPSILON_ANNEAL

# Frequency with which to update the target network, measured in number of
# weight updates
NETWORK_UPDATE_FREQ = 10000

# Number of experiences to train for
NUM_TRAIN_EXPS = 2*REPLAY_MEM_SIZE

# Number of weights to save over training
NUM_WEIGHTS = 10

# Frequency with which to save the weights of the training network, measured
# in number of experiences
WEIGHT_SAVE_FREQ = NUM_TRAIN_EXPS/NUM_WEIGHTS

# Discount factor for Q-learning
DISCOUNT_FACTOR = 0.99

# Dropout probability for network regularisation
DROPOUT = 0.25

# Learning rate for network optimiser
NETWORK_LEARN_RATE = 0.01
