#!/usr/bin/python
from __future__ import print_function
import images
import controllers
import params
import sys
import numpy as np
import datetime


if __name__ == "__main__":
    # print out the current date and time
    time = datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    sys.stderr.write("PYTHON: Current time is: {0}\n".format(time))

    # Get the model name
    model_name = sys.stdin.readline().strip()

    # Get the tag name
    tag = sys.stdin.readline().strip()

    # Get the name of the (optional) weights file
    weights_file = sys.stdin.readline().strip()
    if weights_file == "None":
        weights_file = None

    controller = controllers.TrainingController(model_name, tag, weights_file)

    current_state = None  # initially the current state is empty
    action = None  # Initialise action
    terminal = False  # flag to indicate if episode finished

    elapsed_experiences = 0  # Number of MDP transitions that have occurred
    # Number of times the network weights have been updated
    weight_updates = 0

    # "warm up" the controller  before the main loop
    controllers.warmup_controller(controller)

    # Main MDP loop
    while True:
        # Get the next state
        next_state = np.zeros((params.IMGS_PER_STATE, params.IMG_ROWS,
                               params.IMG_COLS), dtype=np.float32)
        for i in range(0, params.IMGS_PER_STATE):
            img_data_list = images.get_img_data_list()
            img_data_array = images.process_img_data(img_data_list)
            next_state[i] = img_data_array

        # Get other game information
        lives = int(sys.stdin.readline().strip())
        score = int(sys.stdin.readline().strip())
        time = int(sys.stdin.readline().strip())
        sys.stderr.write("PYTHON: Got lives, score, time = {0}, {1}, {2}\n".
                         format(lives, score, time))

        # Check if the current state is a terminal state (no lives left)
        if lives == 0:
            terminal = True

        # Record the experience if possible
        if current_state is not None:
            controller.record_experience(current_state, action, lives,
                                         score, time, next_state, terminal)
            elapsed_experiences += 1
            sys.stderr.write("PYTHON: Number of elapsed experiences = {0}\n".
                             format(elapsed_experiences))

        # Check if an update step on the network should be performed
        if elapsed_experiences > params.NUM_EXPLORE_EXPS:
            # check if the exploration end time needs to be recorded
            if elapsed_experiences == params.NUM_EXPLORE_EXPS + 1:
                explore_end_time = datetime.datetime.now()
            # Update the exploration parameter value
            controller.update_exploration_prob(elapsed_experiences)
            # Do an update on the network weights
            controller.update_network()
            weight_updates += 1

        if weight_updates > 0:
            # Sync the target network if necessary
            if weight_updates % params.NETWORK_UPDATE_FREQ == 0:
                sys.stderr.write("PYTHON: Syncing target network\n")
                controller.sync_target_network()

        # check if the training network weights need to be saved
        if elapsed_experiences != 0 and elapsed_experiences % \
                params.WEIGHT_SAVE_FREQ == 0:
            sys.stderr.write("PYTHON: Saving training network weights\n")
            controller.save_network_weights(False)

        # Update the current state
        current_state = next_state

        # Determine the action to take
        action = controller.select_action(current_state)
        # Send action back to the game
        print(action)
        sys.stdout.flush()

        if terminal:
            sys.stderr.write('PYTHON: Reached a terminal state!\n')
            # reset the controller game info
            sys.stderr.write('PYTHON: Controller is in training mode, '
                             'so resetting the game...\n')
            controller.reset_game_info()
            # reset terminal flag
            terminal = False
