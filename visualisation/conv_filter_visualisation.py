'''Visualization of the filters of VGG16, via gradient ascent in input space.

This script can run on CPU in a few minutes (with the TensorFlow backend).

Results example: http://i.imgur.com/4nj4KjN.jpg
'''
from __future__ import print_function
from scipy.misc import imsave
import numpy as np
import time
from keras import backend as K
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers.core import Flatten, Dense, Dropout
from keras.optimizers import Adam
import argparse
import cv2
from PIL import Image

# dimensions of the generated pictures for each filter.
IMGS_PER_STATE = 3
img_width = 84
img_height = 84

# Dropout probability for network regularisation
DROPOUT = 0.25
# Learning rate for network optimiser
NETWORK_LEARN_RATE = 0.0001


def get_model(model_name):
    model = None
    if model_name == "google_atari_vanilla":
        model = Sequential()
        model.add(Convolution2D(32, 8, 8, subsample=(4, 4), border_mode='same',
                                activation='relu', init='he_normal',
                                input_shape=(IMGS_PER_STATE, img_height,
                                             img_width), name='conv_1'))
        model.add(Convolution2D(64, 4, 4, subsample=(2, 2), border_mode='same',
                                activation='relu', init='he_normal',
                                name='conv_2'))
        model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode='same',
                                activation='relu', init='he_normal',
                                name='conv_3'))
        model.add(Flatten())
        model.add(Dense(512, activation='relu', init='he_normal'))
        model.add(Dense(5, init='he_normal'))
        adam = Adam(lr=NETWORK_LEARN_RATE)
        model.compile(loss='mse', optimizer=adam)
    elif model_name == "google_atari_dropout_fc":
        model = Sequential()
        model.add(Convolution2D(32, 8, 8, subsample=(4, 4), border_mode='same',
                                activation='relu', init='he_normal',
                                input_shape=(IMGS_PER_STATE, img_height,
                                             img_width), name='conv_1'))
        model.add(Convolution2D(64, 4, 4, subsample=(2, 2), border_mode='same',
                                activation='relu', init='he_normal',
                                name='conv_2'))
        model.add(Convolution2D(64, 3, 3, subsample=(1, 1), border_mode='same',
                                activation='relu', init='he_normal',
                                name='conv_3'))
        model.add(Dropout(DROPOUT))
        model.add(Flatten())
        model.add(Dense(512, activation='relu', init='he_normal'))
        model.add(Dropout(DROPOUT))
        model.add(Dense(5, init='he_normal'))
        adam = Adam(lr=NETWORK_LEARN_RATE)
        model.compile(loss='mse', optimizer=adam)
    return model


# util function to convert a tensor into a valid image
def deprocess_image(x):
    # normalize tensor: center on 0., ensure std is 0.1
    x -= x.mean()
    x /= (x.std() + 1e-5)
    x *= 0.1

    # clip to [0, 1]
    x += 0.5
    x = np.clip(x, 0, 1)

    # convert to RGB array
    x *= 255
    if K.image_dim_ordering() == 'th':
        x = x.transpose((1, 2, 0))
    x = np.clip(x, 0, 255).astype('uint8')
    return x


def normalize(x):
    # utility function to normalize a tensor by its L2 norm
    return x / (K.sqrt(K.mean(K.square(x))) + 1e-5)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--iterations", type=int, default=20,
                        help='Number of gradient ascent iterations')
    parser.add_argument("--imgs", type=str, nargs=3,
                        help='Path to the images to project filter on')
    parser.add_argument("--weights_path", type=str, help='Path to network '
                                                         'weights file')
    parser.add_argument("--layer", type=str, default='conv_1',
                        help='Name of layer to use')
    parser.add_argument("--num_filters", type=int, default=32,
                        help='Number of filters to vizualize, starting from '
                             'filter number 0.')
    parser.add_argument("--model_name", type=str,
                        help="Name of the model to use")
    parser.add_argument("--grid_side_length", type=int, default=4,
                        help="Side length of the image filter grid that is "
                             "produced.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":

    args = get_args()
    print(args)

    # build the network
    model = get_model(args.model_name)
    model.load_weights(args.weights_path)
    print('Model loaded.')

    model.summary()

    # this is the placeholder for the input images
    input_img = model.input

    # get the symbolic outputs of each "key" layer (we gave them unique names).
    layer_dict = dict([(layer.name, layer) for layer in model.layers])

    kept_filters = []
    for filter_index in range(0, args.num_filters):
        print('Processing filter %d' % filter_index)
        start_time = time.time()

        # we build a loss function that maximizes the activation
        # of the nth filter of the layer considered
        layer_output = layer_dict[args.layer].output
        if K.image_dim_ordering() == 'th':
            loss = K.mean(layer_output[:, filter_index, :, :])
        else:
            loss = K.mean(layer_output[:, :, :, filter_index])

        # we compute the gradient of the input picture wrt this loss
        grads = K.gradients(loss, input_img)[0]

        # normalization trick: we normalize the gradient
        grads = normalize(grads)

        # this function returns the loss and grads given the input picture
        iterate = K.function([input_img], [loss, grads])

        # step size for gradient ascent
        step = 1.

        input_img_data = None
        if args.imgs is None:
            # we start from a gray image with some random noise
            if K.image_dim_ordering() == 'th':
                input_img_data = np.random.random((1, 3, img_width, img_height))
            else:
                input_img_data = np.random.random((1, img_width, img_height, 3))
            input_img_data = input_img_data.astype('float32')
        else:
            input_img_data = np.zeros((1, IMGS_PER_STATE, img_height,
                                       img_width))
            # Read in the supplied images and glue them together
            for i in range(0, len(args.imgs)):
                img = args.imgs[i]
                img = Image.open(img)
                img = img.convert("L")
                img = img.resize((img_width, img_height), Image.ANTIALIAS)
                img_data_array = np.asarray(img.getdata())
                img_data_array = img_data_array.astype('float32')
                img_data_array = np.reshape(img_data_array, (img_height,
                                                             img_width))/255
                input_img_data[0][i] = img_data_array

        # we run gradient ascent for 20 steps
        for i in range(args.iterations):
            loss_value, grads_value = iterate([input_img_data])
            input_img_data += grads_value * step

            print('Current loss value:', loss_value)
            if loss_value <= 0.:
                # some filters get stuck to 0, we can skip them
                break

        # decode the resulting input image
        if loss_value > 0:
            img = deprocess_image(input_img_data[0])
            kept_filters.append((img, loss_value))
        end_time = time.time()
        print('Filter %d processed in %ds' % (filter_index, end_time -
                                              start_time))

    # we will stich the best 64 filters on a 8 x 8 grid.
    n = args.grid_side_length

    # the filters that have the highest loss are assumed to be better-looking.
    # we will only keep the top 64 filters.
    kept_filters.sort(key=lambda x: x[1], reverse=True)
    kept_filters = kept_filters[:n * n]

    # build a black picture with enough space for
    # our 8 x 8 filters of size 128 x 128, with a 5px margin in between
    margin = 5
    width = n * img_width + (n - 1) * margin
    height = n * img_height + (n - 1) * margin
    stitched_filters = np.zeros((width, height, 3))

    # fill the picture with our saved filters
    for i in range(n):
        for j in range(n):
            img, loss = kept_filters[i * n + j]
            stitched_filters[(img_width + margin) * i: (img_width + margin) * i + img_width,
            (img_height + margin) * j: (img_height + margin) * j + img_height, :] = img

    # save the result to disk
    cv2.imwrite('stitched_filters_{0}_{1}_{2}x{3}.png'.format(args.imgs,
                                                              args.layer, n, n),
                stitched_filters)
