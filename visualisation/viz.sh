#!/bin/bash
# set the number of nodes
#SBATCH --nodes=1

# set max wallclock time
#SBATCH --time=1:00:00

# set parition and gpu info
#SBATCH --partition="gpu"
#SBATCH --gres=gpu:1

model_name="google_atari_vanilla"
weights_path="/home/Student/s4291689/mpvg2012-rl/network_weights/google_atari_vanilla-lr_0.0001-0/google_atari_vanilla-lr_0.0001-0-2016-09-23_21:12:26-weights.h5"

export THEANO_FLAGS="floatX=float32,device=gpu,nvcc.fastmath=True,lib.cnmem=1"

python conv_filter_visualisuation.py --weights_path=${weights_path}
--model_name=${model_name}
