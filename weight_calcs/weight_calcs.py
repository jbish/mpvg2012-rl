#!/usr/bin/python
from __future__ import print_function
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')  # Plot without X server
import matplotlib.pyplot as plt
from scipy import spatial
import sys
sys.path.append("../src")
import network

MODEL_NAME = "google_atari_dropout_fc"
TAG = "drop_25_priority_replay_lr_0.0001_1200k_exp_600k_epsilon"
NUM_DIRS = 3
TITLE = "Priority Replay Agent Weights Evolution (p = 0.75, LR = 0.0001) " \
        "1200k Exps"


def get_flat_weight_layers(weights):
    flat_layer_weights = []
    # Each actual layer has an additional bias layer in the weights array,
    # so loop over the weights array in steps of 2.
    for i in range(0, len(weights), 2):
        input_array = weights[i]
        bias_array = weights[i+1]
        # Flatten the input_array by continually self-concatenating it until
        # it is 1D
        while len(input_array.shape) != 1:
            input_array = np.concatenate(input_array)
        # Now concatenate the input_array with the bias_array and add the
        # result to the flat_layer_weights list
        flat_layer = np.concatenate((input_array, bias_array))
        flat_layer_weights.append(flat_layer)
    return flat_layer_weights


def calc_cosine_sim(weights_flat_layers):
    num_weights, num_layers = weights_flat_layers.shape
    # create a matrix to hold the similarities
    sim_matrix = np.ones((num_weights, num_layers))
    for i in range(1, num_weights):  # don't need to do first set of weights
        # as we don't want to compare first set with itself
        for j in range(0, num_layers):
            init_vector = weights_flat_layers[0][j]
            other_vector = weights_flat_layers[i][j]
            sim = 1 - spatial.distance.cosine(init_vector, other_vector)
            sim_matrix[i][j] = sim
    return sim_matrix


def calc_vector_means(weights_flat_layers):
    num_weights, num_layers = weights_flat_layers.shape
    # Create a matrix to hold the mean vectors
    vector_means = np.zeros((num_weights, num_layers))
    for i in range(0, num_weights):
        for j in range(0, num_layers):
            current_vector = weights_flat_layers[i][j]
            mean = np.mean(current_vector)
            vector_means[i][j] = mean
    return vector_means


def plot_data(sim_matrix, mean_matrix):
    fig = plt.figure(figsize=(8.5, 4.5))
    plt.suptitle(TITLE)
    plot_sim_matrix(sim_matrix, fig)
    plot_mean_matrix(mean_matrix, fig)
    plt.subplots_adjust(wspace=0.4)
    plt.savefig("{0}-{1}-weights_evolution.png".format(MODEL_NAME, TAG))
    #plt.show()


def plot_sim_matrix(sim_matrix, fig):
    ax1 = fig.add_subplot(1, 2, 1)
    greys = plt.get_cmap('coolwarm')
    rows, cols = sim_matrix.shape
    layers = range(1, cols+1)
    for i in range(0, rows):
        ax1.scatter(layers, sim_matrix[i], color=greys(float(i)/(rows-1)))
    ax1.set_xticks(layers)
    ax1.set_ylim(0, 1)
    ax1.set_xlabel("Layer number")
    ax1.set_ylabel("Cosine sim to first layer weight vector")


def plot_mean_matrix(mean_matrix, fig):
    min_val = np.min(mean_matrix)*1.05
    max_val = np.max(mean_matrix)*1.05
    ax2 = fig.add_subplot(1, 2, 2)
    greys = plt.get_cmap('coolwarm')
    rows, cols = mean_matrix.shape
    layers = range(1, cols+1)
    for i in range(0, rows):
        ax2.scatter(layers, mean_matrix[i], color=greys(float(i) / (rows - 1)))
    ax2.set_xticks(layers)
    ax2.set_ylim(bottom=min_val, top=max_val)
    ax2.set_xlabel("Layer number")
    ax2.set_ylabel("Mean of layer weight vector")


if __name__ == "__main__":
    dir_list = \
        ["../network_weights/{0}-{1}-{2}".format(MODEL_NAME, TAG, i) for i in
         range(0, NUM_DIRS)]

    sim_matrices = []  # list to store all the sim matrices for the dirs
    mean_matrices = []  # list to store the mean of each
    # layer's weight vector for each dir

    for directory in dir_list:
        weights_list = [f for f in os.listdir(directory) if f.endswith('h5')]
        weights_list.sort()

        # set init weights and get them back as a numpy array
        init_weights = weights_list[0]
        init_network = network.Convnet(MODEL_NAME)
        init_network.model.load_weights(os.path.join(directory, init_weights))
        init_weights = np.asarray(init_network.model.get_weights())

        # Create a list to store all the flattened layer weight vectors for
        # all the weight files
        flat_all_weight_layers = []

        # Get flat weight vectors for the layers of the init weights
        flat_init_weight_layers = get_flat_weight_layers(init_weights)
        flat_all_weight_layers.append(flat_init_weight_layers)

        # Do the same for all other weights in the directory
        for i in range(1, len(weights_list)):
            current_weights = weights_list[i]
            current_network = network.Convnet(MODEL_NAME)
            current_network.model.load_weights(os.path.join(directory,
                                                            current_weights))
            current_weights = np.asarray(current_network.model.get_weights())
            flat_current_weight_layers = get_flat_weight_layers(current_weights)
            flat_all_weight_layers.append(flat_current_weight_layers)

        flat_all_weight_layers = np.asarray(flat_all_weight_layers)
        # Now we have a numpy array of all the flattened layers for all weights
        # files
        # Calculate the cosine similarity matrix
        sim_matrix = calc_cosine_sim(flat_all_weight_layers)
        sim_matrices.append(sim_matrix)

        # Calculate the mean of the layer vectors
        vector_means = calc_vector_means(flat_all_weight_layers)
        mean_matrices.append(vector_means)

    # Average the sim matrices
    sim_matrices = np.asarray(sim_matrices)
    mean_sim_matrix = np.mean(sim_matrices, axis=0)

    # Average the weight vectors
    mean_matrices = np.asarray(mean_matrices)
    mean_mean_matrix = np.mean(mean_matrices, axis=0)

    # Plot the data
    plot_data(mean_sim_matrix, mean_mean_matrix)
