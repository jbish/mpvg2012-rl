#!/usr/bin/bash
# set the number of nodes
#SBATCH --nodes=1
# set number of cpus
#SBATCH --cpus-per-task=1

# set max wallclock time
#SBATCH --time=1:00:00

# set parition and gpu info
#SBATCH --partition="gpu"
#SBATCH --gres=gpu:1

python weight_calcs.py
